/**
 */
package Tiendavirtual;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.AbstractEnumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>User State</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see Tiendavirtual.TiendavirtualPackage#getUserState()
 * @model
 * @generated
 */
public final class UserState extends AbstractEnumerator {
	/**
	 * The '<em><b>New</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>New</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NEW_LITERAL
	 * @model name="New"
	 * @generated
	 * @ordered
	 */
	public static final int NEW = 0;

	/**
	 * The '<em><b>Active</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Active</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ACTIVE_LITERAL
	 * @model name="Active"
	 * @generated
	 * @ordered
	 */
	public static final int ACTIVE = 1;

	/**
	 * The '<em><b>Blocked</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Blocked</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #BLOCKED_LITERAL
	 * @model name="Blocked"
	 * @generated
	 * @ordered
	 */
	public static final int BLOCKED = 2;

	/**
	 * The '<em><b>Banned</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Banned</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #BANNED_LITERAL
	 * @model name="Banned"
	 * @generated
	 * @ordered
	 */
	public static final int BANNED = 3;

	/**
	 * The '<em><b>New</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NEW
	 * @generated
	 * @ordered
	 */
	public static final UserState NEW_LITERAL = new UserState(NEW, "New", "New");

	/**
	 * The '<em><b>Active</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ACTIVE
	 * @generated
	 * @ordered
	 */
	public static final UserState ACTIVE_LITERAL = new UserState(ACTIVE, "Active", "Active");

	/**
	 * The '<em><b>Blocked</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BLOCKED
	 * @generated
	 * @ordered
	 */
	public static final UserState BLOCKED_LITERAL = new UserState(BLOCKED, "Blocked", "Blocked");

	/**
	 * The '<em><b>Banned</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BANNED
	 * @generated
	 * @ordered
	 */
	public static final UserState BANNED_LITERAL = new UserState(BANNED, "Banned", "Banned");

	/**
	 * An array of all the '<em><b>User State</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final UserState[] VALUES_ARRAY =
		new UserState[] {
			NEW_LITERAL,
			ACTIVE_LITERAL,
			BLOCKED_LITERAL,
			BANNED_LITERAL,
		};

	/**
	 * A public read-only list of all the '<em><b>User State</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>User State</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static UserState get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			UserState result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>User State</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static UserState getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			UserState result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>User State</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static UserState get(int value) {
		switch (value) {
			case NEW: return NEW_LITERAL;
			case ACTIVE: return ACTIVE_LITERAL;
			case BLOCKED: return BLOCKED_LITERAL;
			case BANNED: return BANNED_LITERAL;
		}
		return null;
	}

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private UserState(int value, String name, String literal) {
		super(value, name, literal);
	}

} //UserState
