/**
 */
package Tiendavirtual;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Account</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link Tiendavirtual.Account#getId <em>Id</em>}</li>
 *   <li>{@link Tiendavirtual.Account#getBilling_address <em>Billing address</em>}</li>
 *   <li>{@link Tiendavirtual.Account#getIs_closed <em>Is closed</em>}</li>
 *   <li>{@link Tiendavirtual.Account#getOpen <em>Open</em>}</li>
 *   <li>{@link Tiendavirtual.Account#getClosed <em>Closed</em>}</li>
 *   <li>{@link Tiendavirtual.Account#get_ <em></em>}</li>
 *   <li>{@link Tiendavirtual.Account#getPayment <em>Payment</em>}</li>
 * </ul>
 * </p>
 *
 * @see Tiendavirtual.TiendavirtualPackage#getAccount()
 * @model
 * @generated
 */
public interface Account extends EObject {
	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see Tiendavirtual.TiendavirtualPackage#getAccount_Id()
	 * @model unique="false" dataType="org.eclipse.uml2.types.String" required="true" changeable="false" ordered="false"
	 * @generated
	 */
	String getId();

	/**
	 * Returns the value of the '<em><b>Billing address</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Billing address</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Billing address</em>' attribute.
	 * @see Tiendavirtual.TiendavirtualPackage#getAccount_Billing_address()
	 * @model unique="false" dataType="org.eclipse.uml2.types.String" required="true" changeable="false" ordered="false"
	 * @generated
	 */
	String getBilling_address();

	/**
	 * Returns the value of the '<em><b>Is closed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is closed</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is closed</em>' attribute.
	 * @see Tiendavirtual.TiendavirtualPackage#getAccount_Is_closed()
	 * @model unique="false" dataType="org.eclipse.uml2.types.String" required="true" changeable="false" ordered="false"
	 * @generated
	 */
	String getIs_closed();

	/**
	 * Returns the value of the '<em><b>Open</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Open</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Open</em>' attribute.
	 * @see Tiendavirtual.TiendavirtualPackage#getAccount_Open()
	 * @model unique="false" dataType="org.eclipse.uml2.types.String" required="true" changeable="false" ordered="false"
	 * @generated
	 */
	String getOpen();

	/**
	 * Returns the value of the '<em><b>Closed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Closed</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Closed</em>' attribute.
	 * @see Tiendavirtual.TiendavirtualPackage#getAccount_Closed()
	 * @model unique="false" dataType="org.eclipse.uml2.types.String" required="true" changeable="false" ordered="false"
	 * @generated
	 */
	String getClosed();

	/**
	 * Returns the value of the '<em><b></b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em></em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em></em>' containment reference.
	 * @see Tiendavirtual.TiendavirtualPackage#getAccount__()
	 * @model containment="true" required="true" changeable="false" ordered="false"
	 * @generated
	 */
	ShoppingCart get_();

	/**
	 * Returns the value of the '<em><b>Payment</b></em>' reference list.
	 * The list contents are of type {@link Tiendavirtual.Payment}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Payment</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Payment</em>' reference list.
	 * @see Tiendavirtual.TiendavirtualPackage#getAccount_Payment()
	 * @model type="Tiendavirtual.Payment" changeable="false" ordered="false"
	 * @generated
	 */
	EList getPayment();

} // Account
