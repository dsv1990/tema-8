/**
 */
package Tiendavirtual;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see Tiendavirtual.TiendavirtualFactory
 * @model kind="package"
 * @generated
 */
public interface TiendavirtualPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "Tiendavirtual";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///Tiendavirtual.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "Tiendavirtual";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TiendavirtualPackage eINSTANCE = Tiendavirtual.impl.TiendavirtualPackageImpl.init();

	/**
	 * The meta object id for the '{@link Tiendavirtual.impl.WebUserImpl <em>Web User</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Tiendavirtual.impl.WebUserImpl
	 * @see Tiendavirtual.impl.TiendavirtualPackageImpl#getWebUser()
	 * @generated
	 */
	int WEB_USER = 0;

	/**
	 * The feature id for the '<em><b>Login id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WEB_USER__LOGIN_ID = 0;

	/**
	 * The feature id for the '<em><b>Password</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WEB_USER__PASSWORD = 1;

	/**
	 * The feature id for the '<em><b>State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WEB_USER__STATE = 2;

	/**
	 * The feature id for the '<em><b></b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WEB_USER__ = 3;

	/**
	 * The number of structural features of the '<em>Web User</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WEB_USER_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link Tiendavirtual.impl.ShoppingCartImpl <em>Shopping Cart</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Tiendavirtual.impl.ShoppingCartImpl
	 * @see Tiendavirtual.impl.TiendavirtualPackageImpl#getShoppingCart()
	 * @generated
	 */
	int SHOPPING_CART = 1;

	/**
	 * The feature id for the '<em><b>Created</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHOPPING_CART__CREATED = 0;

	/**
	 * The feature id for the '<em><b></b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHOPPING_CART__ = 1;

	/**
	 * The number of structural features of the '<em>Shopping Cart</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHOPPING_CART_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link Tiendavirtual.impl.LineItemImpl <em>Line Item</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Tiendavirtual.impl.LineItemImpl
	 * @see Tiendavirtual.impl.TiendavirtualPackageImpl#getLineItem()
	 * @generated
	 */
	int LINE_ITEM = 2;

	/**
	 * The feature id for the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINE_ITEM__QUANTITY = 0;

	/**
	 * The feature id for the '<em><b>Price</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINE_ITEM__PRICE = 1;

	/**
	 * The feature id for the '<em><b></b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINE_ITEM__ = 2;

	/**
	 * The number of structural features of the '<em>Line Item</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINE_ITEM_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link Tiendavirtual.impl.ProductImpl <em>Product</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Tiendavirtual.impl.ProductImpl
	 * @see Tiendavirtual.impl.TiendavirtualPackageImpl#getProduct()
	 * @generated
	 */
	int PRODUCT = 3;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRODUCT__ID = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRODUCT__NAME = 1;

	/**
	 * The feature id for the '<em><b>Supplier</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRODUCT__SUPPLIER = 2;

	/**
	 * The number of structural features of the '<em>Product</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRODUCT_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link Tiendavirtual.impl.CustomerImpl <em>Customer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Tiendavirtual.impl.CustomerImpl
	 * @see Tiendavirtual.impl.TiendavirtualPackageImpl#getCustomer()
	 * @generated
	 */
	int CUSTOMER = 4;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOMER__ID = 0;

	/**
	 * The feature id for the '<em><b>Address</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOMER__ADDRESS = 1;

	/**
	 * The feature id for the '<em><b>Phone</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOMER__PHONE = 2;

	/**
	 * The feature id for the '<em><b>Email</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOMER__EMAIL = 3;

	/**
	 * The feature id for the '<em><b></b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOMER__ = 4;

	/**
	 * The number of structural features of the '<em>Customer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOMER_FEATURE_COUNT = 5;

	/**
	 * The meta object id for the '{@link Tiendavirtual.impl.AccountImpl <em>Account</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Tiendavirtual.impl.AccountImpl
	 * @see Tiendavirtual.impl.TiendavirtualPackageImpl#getAccount()
	 * @generated
	 */
	int ACCOUNT = 5;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCOUNT__ID = 0;

	/**
	 * The feature id for the '<em><b>Billing address</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCOUNT__BILLING_ADDRESS = 1;

	/**
	 * The feature id for the '<em><b>Is closed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCOUNT__IS_CLOSED = 2;

	/**
	 * The feature id for the '<em><b>Open</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCOUNT__OPEN = 3;

	/**
	 * The feature id for the '<em><b>Closed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCOUNT__CLOSED = 4;

	/**
	 * The feature id for the '<em><b></b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCOUNT__ = 5;

	/**
	 * The feature id for the '<em><b>Payment</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCOUNT__PAYMENT = 6;

	/**
	 * The number of structural features of the '<em>Account</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCOUNT_FEATURE_COUNT = 7;

	/**
	 * The meta object id for the '{@link Tiendavirtual.impl.OrderImpl <em>Order</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Tiendavirtual.impl.OrderImpl
	 * @see Tiendavirtual.impl.TiendavirtualPackageImpl#getOrder()
	 * @generated
	 */
	int ORDER = 6;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORDER__NUMBER = 0;

	/**
	 * The feature id for the '<em><b>Ordered</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORDER__ORDERED = 1;

	/**
	 * The feature id for the '<em><b>Shipped</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORDER__SHIPPED = 2;

	/**
	 * The feature id for the '<em><b>Ship to</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORDER__SHIP_TO = 3;

	/**
	 * The feature id for the '<em><b>Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORDER__STATUS = 4;

	/**
	 * The feature id for the '<em><b>Total</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORDER__TOTAL = 5;

	/**
	 * The feature id for the '<em><b></b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORDER__ = 6;

	/**
	 * The number of structural features of the '<em>Order</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORDER_FEATURE_COUNT = 7;

	/**
	 * The meta object id for the '{@link Tiendavirtual.impl.PaymentImpl <em>Payment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Tiendavirtual.impl.PaymentImpl
	 * @see Tiendavirtual.impl.TiendavirtualPackageImpl#getPayment()
	 * @generated
	 */
	int PAYMENT = 7;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAYMENT__ID = 0;

	/**
	 * The feature id for the '<em><b>Paid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAYMENT__PAID = 1;

	/**
	 * The feature id for the '<em><b>Total</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAYMENT__TOTAL = 2;

	/**
	 * The feature id for the '<em><b>Details</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAYMENT__DETAILS = 3;

	/**
	 * The number of structural features of the '<em>Payment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAYMENT_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link Tiendavirtual.UserState <em>User State</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Tiendavirtual.UserState
	 * @see Tiendavirtual.impl.TiendavirtualPackageImpl#getUserState()
	 * @generated
	 */
	int USER_STATE = 8;

	/**
	 * The meta object id for the '{@link Tiendavirtual.OrderStatus <em>Order Status</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Tiendavirtual.OrderStatus
	 * @see Tiendavirtual.impl.TiendavirtualPackageImpl#getOrderStatus()
	 * @generated
	 */
	int ORDER_STATUS = 9;


	/**
	 * Returns the meta object for class '{@link Tiendavirtual.WebUser <em>Web User</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Web User</em>'.
	 * @see Tiendavirtual.WebUser
	 * @generated
	 */
	EClass getWebUser();

	/**
	 * Returns the meta object for the attribute '{@link Tiendavirtual.WebUser#getLogin_id <em>Login id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Login id</em>'.
	 * @see Tiendavirtual.WebUser#getLogin_id()
	 * @see #getWebUser()
	 * @generated
	 */
	EAttribute getWebUser_Login_id();

	/**
	 * Returns the meta object for the attribute '{@link Tiendavirtual.WebUser#getPassword <em>Password</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Password</em>'.
	 * @see Tiendavirtual.WebUser#getPassword()
	 * @see #getWebUser()
	 * @generated
	 */
	EAttribute getWebUser_Password();

	/**
	 * Returns the meta object for the attribute '{@link Tiendavirtual.WebUser#getState <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>State</em>'.
	 * @see Tiendavirtual.WebUser#getState()
	 * @see #getWebUser()
	 * @generated
	 */
	EAttribute getWebUser_State();

	/**
	 * Returns the meta object for the reference '{@link Tiendavirtual.WebUser#get_ <em></em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em></em>'.
	 * @see Tiendavirtual.WebUser#get_()
	 * @see #getWebUser()
	 * @generated
	 */
	EReference getWebUser__();

	/**
	 * Returns the meta object for class '{@link Tiendavirtual.ShoppingCart <em>Shopping Cart</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Shopping Cart</em>'.
	 * @see Tiendavirtual.ShoppingCart
	 * @generated
	 */
	EClass getShoppingCart();

	/**
	 * Returns the meta object for the attribute '{@link Tiendavirtual.ShoppingCart#getCreated <em>Created</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Created</em>'.
	 * @see Tiendavirtual.ShoppingCart#getCreated()
	 * @see #getShoppingCart()
	 * @generated
	 */
	EAttribute getShoppingCart_Created();

	/**
	 * Returns the meta object for the reference list '{@link Tiendavirtual.ShoppingCart#get_ <em></em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em></em>'.
	 * @see Tiendavirtual.ShoppingCart#get_()
	 * @see #getShoppingCart()
	 * @generated
	 */
	EReference getShoppingCart__();

	/**
	 * Returns the meta object for class '{@link Tiendavirtual.LineItem <em>Line Item</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Line Item</em>'.
	 * @see Tiendavirtual.LineItem
	 * @generated
	 */
	EClass getLineItem();

	/**
	 * Returns the meta object for the attribute '{@link Tiendavirtual.LineItem#getQuantity <em>Quantity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quantity</em>'.
	 * @see Tiendavirtual.LineItem#getQuantity()
	 * @see #getLineItem()
	 * @generated
	 */
	EAttribute getLineItem_Quantity();

	/**
	 * Returns the meta object for the attribute '{@link Tiendavirtual.LineItem#getPrice <em>Price</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Price</em>'.
	 * @see Tiendavirtual.LineItem#getPrice()
	 * @see #getLineItem()
	 * @generated
	 */
	EAttribute getLineItem_Price();

	/**
	 * Returns the meta object for the reference '{@link Tiendavirtual.LineItem#get_ <em></em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em></em>'.
	 * @see Tiendavirtual.LineItem#get_()
	 * @see #getLineItem()
	 * @generated
	 */
	EReference getLineItem__();

	/**
	 * Returns the meta object for class '{@link Tiendavirtual.Product <em>Product</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Product</em>'.
	 * @see Tiendavirtual.Product
	 * @generated
	 */
	EClass getProduct();

	/**
	 * Returns the meta object for the attribute '{@link Tiendavirtual.Product#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see Tiendavirtual.Product#getId()
	 * @see #getProduct()
	 * @generated
	 */
	EAttribute getProduct_Id();

	/**
	 * Returns the meta object for the attribute '{@link Tiendavirtual.Product#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see Tiendavirtual.Product#getName()
	 * @see #getProduct()
	 * @generated
	 */
	EAttribute getProduct_Name();

	/**
	 * Returns the meta object for the attribute '{@link Tiendavirtual.Product#getSupplier <em>Supplier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Supplier</em>'.
	 * @see Tiendavirtual.Product#getSupplier()
	 * @see #getProduct()
	 * @generated
	 */
	EAttribute getProduct_Supplier();

	/**
	 * Returns the meta object for class '{@link Tiendavirtual.Customer <em>Customer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Customer</em>'.
	 * @see Tiendavirtual.Customer
	 * @generated
	 */
	EClass getCustomer();

	/**
	 * Returns the meta object for the attribute '{@link Tiendavirtual.Customer#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see Tiendavirtual.Customer#getId()
	 * @see #getCustomer()
	 * @generated
	 */
	EAttribute getCustomer_Id();

	/**
	 * Returns the meta object for the attribute '{@link Tiendavirtual.Customer#getAddress <em>Address</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Address</em>'.
	 * @see Tiendavirtual.Customer#getAddress()
	 * @see #getCustomer()
	 * @generated
	 */
	EAttribute getCustomer_Address();

	/**
	 * Returns the meta object for the attribute '{@link Tiendavirtual.Customer#getPhone <em>Phone</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Phone</em>'.
	 * @see Tiendavirtual.Customer#getPhone()
	 * @see #getCustomer()
	 * @generated
	 */
	EAttribute getCustomer_Phone();

	/**
	 * Returns the meta object for the attribute '{@link Tiendavirtual.Customer#getEmail <em>Email</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Email</em>'.
	 * @see Tiendavirtual.Customer#getEmail()
	 * @see #getCustomer()
	 * @generated
	 */
	EAttribute getCustomer_Email();

	/**
	 * Returns the meta object for the containment reference '{@link Tiendavirtual.Customer#get_ <em></em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em></em>'.
	 * @see Tiendavirtual.Customer#get_()
	 * @see #getCustomer()
	 * @generated
	 */
	EReference getCustomer__();

	/**
	 * Returns the meta object for class '{@link Tiendavirtual.Account <em>Account</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Account</em>'.
	 * @see Tiendavirtual.Account
	 * @generated
	 */
	EClass getAccount();

	/**
	 * Returns the meta object for the attribute '{@link Tiendavirtual.Account#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see Tiendavirtual.Account#getId()
	 * @see #getAccount()
	 * @generated
	 */
	EAttribute getAccount_Id();

	/**
	 * Returns the meta object for the attribute '{@link Tiendavirtual.Account#getBilling_address <em>Billing address</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Billing address</em>'.
	 * @see Tiendavirtual.Account#getBilling_address()
	 * @see #getAccount()
	 * @generated
	 */
	EAttribute getAccount_Billing_address();

	/**
	 * Returns the meta object for the attribute '{@link Tiendavirtual.Account#getIs_closed <em>Is closed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is closed</em>'.
	 * @see Tiendavirtual.Account#getIs_closed()
	 * @see #getAccount()
	 * @generated
	 */
	EAttribute getAccount_Is_closed();

	/**
	 * Returns the meta object for the attribute '{@link Tiendavirtual.Account#getOpen <em>Open</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Open</em>'.
	 * @see Tiendavirtual.Account#getOpen()
	 * @see #getAccount()
	 * @generated
	 */
	EAttribute getAccount_Open();

	/**
	 * Returns the meta object for the attribute '{@link Tiendavirtual.Account#getClosed <em>Closed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Closed</em>'.
	 * @see Tiendavirtual.Account#getClosed()
	 * @see #getAccount()
	 * @generated
	 */
	EAttribute getAccount_Closed();

	/**
	 * Returns the meta object for the containment reference '{@link Tiendavirtual.Account#get_ <em></em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em></em>'.
	 * @see Tiendavirtual.Account#get_()
	 * @see #getAccount()
	 * @generated
	 */
	EReference getAccount__();

	/**
	 * Returns the meta object for the reference list '{@link Tiendavirtual.Account#getPayment <em>Payment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Payment</em>'.
	 * @see Tiendavirtual.Account#getPayment()
	 * @see #getAccount()
	 * @generated
	 */
	EReference getAccount_Payment();

	/**
	 * Returns the meta object for class '{@link Tiendavirtual.Order <em>Order</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Order</em>'.
	 * @see Tiendavirtual.Order
	 * @generated
	 */
	EClass getOrder();

	/**
	 * Returns the meta object for the attribute '{@link Tiendavirtual.Order#getNumber <em>Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Number</em>'.
	 * @see Tiendavirtual.Order#getNumber()
	 * @see #getOrder()
	 * @generated
	 */
	EAttribute getOrder_Number();

	/**
	 * Returns the meta object for the attribute '{@link Tiendavirtual.Order#getOrdered <em>Ordered</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ordered</em>'.
	 * @see Tiendavirtual.Order#getOrdered()
	 * @see #getOrder()
	 * @generated
	 */
	EAttribute getOrder_Ordered();

	/**
	 * Returns the meta object for the attribute '{@link Tiendavirtual.Order#getShipped <em>Shipped</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Shipped</em>'.
	 * @see Tiendavirtual.Order#getShipped()
	 * @see #getOrder()
	 * @generated
	 */
	EAttribute getOrder_Shipped();

	/**
	 * Returns the meta object for the attribute '{@link Tiendavirtual.Order#getShip_to <em>Ship to</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ship to</em>'.
	 * @see Tiendavirtual.Order#getShip_to()
	 * @see #getOrder()
	 * @generated
	 */
	EAttribute getOrder_Ship_to();

	/**
	 * Returns the meta object for the attribute '{@link Tiendavirtual.Order#getStatus <em>Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Status</em>'.
	 * @see Tiendavirtual.Order#getStatus()
	 * @see #getOrder()
	 * @generated
	 */
	EAttribute getOrder_Status();

	/**
	 * Returns the meta object for the attribute '{@link Tiendavirtual.Order#getTotal <em>Total</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Total</em>'.
	 * @see Tiendavirtual.Order#getTotal()
	 * @see #getOrder()
	 * @generated
	 */
	EAttribute getOrder_Total();

	/**
	 * Returns the meta object for the reference list '{@link Tiendavirtual.Order#get_ <em></em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em></em>'.
	 * @see Tiendavirtual.Order#get_()
	 * @see #getOrder()
	 * @generated
	 */
	EReference getOrder__();

	/**
	 * Returns the meta object for class '{@link Tiendavirtual.Payment <em>Payment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Payment</em>'.
	 * @see Tiendavirtual.Payment
	 * @generated
	 */
	EClass getPayment();

	/**
	 * Returns the meta object for the attribute '{@link Tiendavirtual.Payment#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see Tiendavirtual.Payment#getId()
	 * @see #getPayment()
	 * @generated
	 */
	EAttribute getPayment_Id();

	/**
	 * Returns the meta object for the attribute '{@link Tiendavirtual.Payment#getPaid <em>Paid</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Paid</em>'.
	 * @see Tiendavirtual.Payment#getPaid()
	 * @see #getPayment()
	 * @generated
	 */
	EAttribute getPayment_Paid();

	/**
	 * Returns the meta object for the attribute '{@link Tiendavirtual.Payment#getTotal <em>Total</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Total</em>'.
	 * @see Tiendavirtual.Payment#getTotal()
	 * @see #getPayment()
	 * @generated
	 */
	EAttribute getPayment_Total();

	/**
	 * Returns the meta object for the attribute '{@link Tiendavirtual.Payment#getDetails <em>Details</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Details</em>'.
	 * @see Tiendavirtual.Payment#getDetails()
	 * @see #getPayment()
	 * @generated
	 */
	EAttribute getPayment_Details();

	/**
	 * Returns the meta object for enum '{@link Tiendavirtual.UserState <em>User State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>User State</em>'.
	 * @see Tiendavirtual.UserState
	 * @generated
	 */
	EEnum getUserState();

	/**
	 * Returns the meta object for enum '{@link Tiendavirtual.OrderStatus <em>Order Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Order Status</em>'.
	 * @see Tiendavirtual.OrderStatus
	 * @generated
	 */
	EEnum getOrderStatus();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	TiendavirtualFactory getTiendavirtualFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link Tiendavirtual.impl.WebUserImpl <em>Web User</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Tiendavirtual.impl.WebUserImpl
		 * @see Tiendavirtual.impl.TiendavirtualPackageImpl#getWebUser()
		 * @generated
		 */
		EClass WEB_USER = eINSTANCE.getWebUser();

		/**
		 * The meta object literal for the '<em><b>Login id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute WEB_USER__LOGIN_ID = eINSTANCE.getWebUser_Login_id();

		/**
		 * The meta object literal for the '<em><b>Password</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute WEB_USER__PASSWORD = eINSTANCE.getWebUser_Password();

		/**
		 * The meta object literal for the '<em><b>State</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute WEB_USER__STATE = eINSTANCE.getWebUser_State();

		/**
		 * The meta object literal for the '<em><b></b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WEB_USER__ = eINSTANCE.getWebUser__();

		/**
		 * The meta object literal for the '{@link Tiendavirtual.impl.ShoppingCartImpl <em>Shopping Cart</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Tiendavirtual.impl.ShoppingCartImpl
		 * @see Tiendavirtual.impl.TiendavirtualPackageImpl#getShoppingCart()
		 * @generated
		 */
		EClass SHOPPING_CART = eINSTANCE.getShoppingCart();

		/**
		 * The meta object literal for the '<em><b>Created</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SHOPPING_CART__CREATED = eINSTANCE.getShoppingCart_Created();

		/**
		 * The meta object literal for the '<em><b></b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SHOPPING_CART__ = eINSTANCE.getShoppingCart__();

		/**
		 * The meta object literal for the '{@link Tiendavirtual.impl.LineItemImpl <em>Line Item</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Tiendavirtual.impl.LineItemImpl
		 * @see Tiendavirtual.impl.TiendavirtualPackageImpl#getLineItem()
		 * @generated
		 */
		EClass LINE_ITEM = eINSTANCE.getLineItem();

		/**
		 * The meta object literal for the '<em><b>Quantity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LINE_ITEM__QUANTITY = eINSTANCE.getLineItem_Quantity();

		/**
		 * The meta object literal for the '<em><b>Price</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LINE_ITEM__PRICE = eINSTANCE.getLineItem_Price();

		/**
		 * The meta object literal for the '<em><b></b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LINE_ITEM__ = eINSTANCE.getLineItem__();

		/**
		 * The meta object literal for the '{@link Tiendavirtual.impl.ProductImpl <em>Product</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Tiendavirtual.impl.ProductImpl
		 * @see Tiendavirtual.impl.TiendavirtualPackageImpl#getProduct()
		 * @generated
		 */
		EClass PRODUCT = eINSTANCE.getProduct();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PRODUCT__ID = eINSTANCE.getProduct_Id();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PRODUCT__NAME = eINSTANCE.getProduct_Name();

		/**
		 * The meta object literal for the '<em><b>Supplier</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PRODUCT__SUPPLIER = eINSTANCE.getProduct_Supplier();

		/**
		 * The meta object literal for the '{@link Tiendavirtual.impl.CustomerImpl <em>Customer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Tiendavirtual.impl.CustomerImpl
		 * @see Tiendavirtual.impl.TiendavirtualPackageImpl#getCustomer()
		 * @generated
		 */
		EClass CUSTOMER = eINSTANCE.getCustomer();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CUSTOMER__ID = eINSTANCE.getCustomer_Id();

		/**
		 * The meta object literal for the '<em><b>Address</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CUSTOMER__ADDRESS = eINSTANCE.getCustomer_Address();

		/**
		 * The meta object literal for the '<em><b>Phone</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CUSTOMER__PHONE = eINSTANCE.getCustomer_Phone();

		/**
		 * The meta object literal for the '<em><b>Email</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CUSTOMER__EMAIL = eINSTANCE.getCustomer_Email();

		/**
		 * The meta object literal for the '<em><b></b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CUSTOMER__ = eINSTANCE.getCustomer__();

		/**
		 * The meta object literal for the '{@link Tiendavirtual.impl.AccountImpl <em>Account</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Tiendavirtual.impl.AccountImpl
		 * @see Tiendavirtual.impl.TiendavirtualPackageImpl#getAccount()
		 * @generated
		 */
		EClass ACCOUNT = eINSTANCE.getAccount();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACCOUNT__ID = eINSTANCE.getAccount_Id();

		/**
		 * The meta object literal for the '<em><b>Billing address</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACCOUNT__BILLING_ADDRESS = eINSTANCE.getAccount_Billing_address();

		/**
		 * The meta object literal for the '<em><b>Is closed</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACCOUNT__IS_CLOSED = eINSTANCE.getAccount_Is_closed();

		/**
		 * The meta object literal for the '<em><b>Open</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACCOUNT__OPEN = eINSTANCE.getAccount_Open();

		/**
		 * The meta object literal for the '<em><b>Closed</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACCOUNT__CLOSED = eINSTANCE.getAccount_Closed();

		/**
		 * The meta object literal for the '<em><b></b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACCOUNT__ = eINSTANCE.getAccount__();

		/**
		 * The meta object literal for the '<em><b>Payment</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACCOUNT__PAYMENT = eINSTANCE.getAccount_Payment();

		/**
		 * The meta object literal for the '{@link Tiendavirtual.impl.OrderImpl <em>Order</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Tiendavirtual.impl.OrderImpl
		 * @see Tiendavirtual.impl.TiendavirtualPackageImpl#getOrder()
		 * @generated
		 */
		EClass ORDER = eINSTANCE.getOrder();

		/**
		 * The meta object literal for the '<em><b>Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ORDER__NUMBER = eINSTANCE.getOrder_Number();

		/**
		 * The meta object literal for the '<em><b>Ordered</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ORDER__ORDERED = eINSTANCE.getOrder_Ordered();

		/**
		 * The meta object literal for the '<em><b>Shipped</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ORDER__SHIPPED = eINSTANCE.getOrder_Shipped();

		/**
		 * The meta object literal for the '<em><b>Ship to</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ORDER__SHIP_TO = eINSTANCE.getOrder_Ship_to();

		/**
		 * The meta object literal for the '<em><b>Status</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ORDER__STATUS = eINSTANCE.getOrder_Status();

		/**
		 * The meta object literal for the '<em><b>Total</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ORDER__TOTAL = eINSTANCE.getOrder_Total();

		/**
		 * The meta object literal for the '<em><b></b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ORDER__ = eINSTANCE.getOrder__();

		/**
		 * The meta object literal for the '{@link Tiendavirtual.impl.PaymentImpl <em>Payment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Tiendavirtual.impl.PaymentImpl
		 * @see Tiendavirtual.impl.TiendavirtualPackageImpl#getPayment()
		 * @generated
		 */
		EClass PAYMENT = eINSTANCE.getPayment();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PAYMENT__ID = eINSTANCE.getPayment_Id();

		/**
		 * The meta object literal for the '<em><b>Paid</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PAYMENT__PAID = eINSTANCE.getPayment_Paid();

		/**
		 * The meta object literal for the '<em><b>Total</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PAYMENT__TOTAL = eINSTANCE.getPayment_Total();

		/**
		 * The meta object literal for the '<em><b>Details</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PAYMENT__DETAILS = eINSTANCE.getPayment_Details();

		/**
		 * The meta object literal for the '{@link Tiendavirtual.UserState <em>User State</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Tiendavirtual.UserState
		 * @see Tiendavirtual.impl.TiendavirtualPackageImpl#getUserState()
		 * @generated
		 */
		EEnum USER_STATE = eINSTANCE.getUserState();

		/**
		 * The meta object literal for the '{@link Tiendavirtual.OrderStatus <em>Order Status</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Tiendavirtual.OrderStatus
		 * @see Tiendavirtual.impl.TiendavirtualPackageImpl#getOrderStatus()
		 * @generated
		 */
		EEnum ORDER_STATUS = eINSTANCE.getOrderStatus();

	}

} //TiendavirtualPackage
