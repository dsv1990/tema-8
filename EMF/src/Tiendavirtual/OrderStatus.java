/**
 */
package Tiendavirtual;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.AbstractEnumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Order Status</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see Tiendavirtual.TiendavirtualPackage#getOrderStatus()
 * @model
 * @generated
 */
public final class OrderStatus extends AbstractEnumerator {
	/**
	 * The '<em><b>New</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>New</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NEW_LITERAL
	 * @model name="New"
	 * @generated
	 * @ordered
	 */
	public static final int NEW = 0;

	/**
	 * The '<em><b>Hold</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Hold</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #HOLD_LITERAL
	 * @model name="Hold"
	 * @generated
	 * @ordered
	 */
	public static final int HOLD = 1;

	/**
	 * The '<em><b>Shipped</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Shipped</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SHIPPED_LITERAL
	 * @model name="Shipped"
	 * @generated
	 * @ordered
	 */
	public static final int SHIPPED = 2;

	/**
	 * The '<em><b>Delivered</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Delivered</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DELIVERED_LITERAL
	 * @model name="Delivered"
	 * @generated
	 * @ordered
	 */
	public static final int DELIVERED = 3;

	/**
	 * The '<em><b>Closed</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Closed</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CLOSED_LITERAL
	 * @model name="Closed"
	 * @generated
	 * @ordered
	 */
	public static final int CLOSED = 4;

	/**
	 * The '<em><b>New</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NEW
	 * @generated
	 * @ordered
	 */
	public static final OrderStatus NEW_LITERAL = new OrderStatus(NEW, "New", "New");

	/**
	 * The '<em><b>Hold</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #HOLD
	 * @generated
	 * @ordered
	 */
	public static final OrderStatus HOLD_LITERAL = new OrderStatus(HOLD, "Hold", "Hold");

	/**
	 * The '<em><b>Shipped</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SHIPPED
	 * @generated
	 * @ordered
	 */
	public static final OrderStatus SHIPPED_LITERAL = new OrderStatus(SHIPPED, "Shipped", "Shipped");

	/**
	 * The '<em><b>Delivered</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DELIVERED
	 * @generated
	 * @ordered
	 */
	public static final OrderStatus DELIVERED_LITERAL = new OrderStatus(DELIVERED, "Delivered", "Delivered");

	/**
	 * The '<em><b>Closed</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CLOSED
	 * @generated
	 * @ordered
	 */
	public static final OrderStatus CLOSED_LITERAL = new OrderStatus(CLOSED, "Closed", "Closed");

	/**
	 * An array of all the '<em><b>Order Status</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final OrderStatus[] VALUES_ARRAY =
		new OrderStatus[] {
			NEW_LITERAL,
			HOLD_LITERAL,
			SHIPPED_LITERAL,
			DELIVERED_LITERAL,
			CLOSED_LITERAL,
		};

	/**
	 * A public read-only list of all the '<em><b>Order Status</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Order Status</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static OrderStatus get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			OrderStatus result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Order Status</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static OrderStatus getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			OrderStatus result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Order Status</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static OrderStatus get(int value) {
		switch (value) {
			case NEW: return NEW_LITERAL;
			case HOLD: return HOLD_LITERAL;
			case SHIPPED: return SHIPPED_LITERAL;
			case DELIVERED: return DELIVERED_LITERAL;
			case CLOSED: return CLOSED_LITERAL;
		}
		return null;
	}

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private OrderStatus(int value, String name, String literal) {
		super(value, name, literal);
	}

} //OrderStatus
