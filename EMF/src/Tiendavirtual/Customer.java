/**
 */
package Tiendavirtual;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Customer</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link Tiendavirtual.Customer#getId <em>Id</em>}</li>
 *   <li>{@link Tiendavirtual.Customer#getAddress <em>Address</em>}</li>
 *   <li>{@link Tiendavirtual.Customer#getPhone <em>Phone</em>}</li>
 *   <li>{@link Tiendavirtual.Customer#getEmail <em>Email</em>}</li>
 *   <li>{@link Tiendavirtual.Customer#get_ <em></em>}</li>
 * </ul>
 * </p>
 *
 * @see Tiendavirtual.TiendavirtualPackage#getCustomer()
 * @model
 * @generated
 */
public interface Customer extends EObject {
	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see Tiendavirtual.TiendavirtualPackage#getCustomer_Id()
	 * @model unique="false" dataType="org.eclipse.uml2.types.String" required="true" changeable="false" ordered="false"
	 * @generated
	 */
	String getId();

	/**
	 * Returns the value of the '<em><b>Address</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Address</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Address</em>' attribute.
	 * @see Tiendavirtual.TiendavirtualPackage#getCustomer_Address()
	 * @model unique="false" dataType="org.eclipse.uml2.types.String" required="true" changeable="false" ordered="false"
	 * @generated
	 */
	String getAddress();

	/**
	 * Returns the value of the '<em><b>Phone</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Phone</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Phone</em>' attribute.
	 * @see Tiendavirtual.TiendavirtualPackage#getCustomer_Phone()
	 * @model unique="false" dataType="org.eclipse.uml2.types.String" required="true" changeable="false" ordered="false"
	 * @generated
	 */
	String getPhone();

	/**
	 * Returns the value of the '<em><b>Email</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Email</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Email</em>' attribute.
	 * @see Tiendavirtual.TiendavirtualPackage#getCustomer_Email()
	 * @model unique="false" dataType="org.eclipse.uml2.types.String" required="true" changeable="false" ordered="false"
	 * @generated
	 */
	String getEmail();

	/**
	 * Returns the value of the '<em><b></b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em></em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em></em>' containment reference.
	 * @see Tiendavirtual.TiendavirtualPackage#getCustomer__()
	 * @model containment="true" required="true" changeable="false" ordered="false"
	 * @generated
	 */
	Account get_();

} // Customer
