/**
 */
package Tiendavirtual;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Line Item</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link Tiendavirtual.LineItem#getQuantity <em>Quantity</em>}</li>
 *   <li>{@link Tiendavirtual.LineItem#getPrice <em>Price</em>}</li>
 *   <li>{@link Tiendavirtual.LineItem#get_ <em></em>}</li>
 * </ul>
 * </p>
 *
 * @see Tiendavirtual.TiendavirtualPackage#getLineItem()
 * @model
 * @generated
 */
public interface LineItem extends EObject {
	/**
	 * Returns the value of the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Quantity</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Quantity</em>' attribute.
	 * @see Tiendavirtual.TiendavirtualPackage#getLineItem_Quantity()
	 * @model unique="false" dataType="org.eclipse.uml2.types.String" required="true" changeable="false" ordered="false"
	 * @generated
	 */
	String getQuantity();

	/**
	 * Returns the value of the '<em><b>Price</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Price</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Price</em>' attribute.
	 * @see Tiendavirtual.TiendavirtualPackage#getLineItem_Price()
	 * @model unique="false" dataType="org.eclipse.uml2.types.String" required="true" changeable="false" ordered="false"
	 * @generated
	 */
	String getPrice();

	/**
	 * Returns the value of the '<em><b></b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em></em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em></em>' reference.
	 * @see Tiendavirtual.TiendavirtualPackage#getLineItem__()
	 * @model required="true" changeable="false" ordered="false"
	 * @generated
	 */
	Product get_();

} // LineItem
