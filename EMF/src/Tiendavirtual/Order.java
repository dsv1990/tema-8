/**
 */
package Tiendavirtual;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Order</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link Tiendavirtual.Order#getNumber <em>Number</em>}</li>
 *   <li>{@link Tiendavirtual.Order#getOrdered <em>Ordered</em>}</li>
 *   <li>{@link Tiendavirtual.Order#getShipped <em>Shipped</em>}</li>
 *   <li>{@link Tiendavirtual.Order#getShip_to <em>Ship to</em>}</li>
 *   <li>{@link Tiendavirtual.Order#getStatus <em>Status</em>}</li>
 *   <li>{@link Tiendavirtual.Order#getTotal <em>Total</em>}</li>
 *   <li>{@link Tiendavirtual.Order#get_ <em></em>}</li>
 * </ul>
 * </p>
 *
 * @see Tiendavirtual.TiendavirtualPackage#getOrder()
 * @model
 * @generated
 */
public interface Order extends EObject {
	/**
	 * Returns the value of the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Number</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Number</em>' attribute.
	 * @see Tiendavirtual.TiendavirtualPackage#getOrder_Number()
	 * @model unique="false" dataType="org.eclipse.uml2.types.String" required="true" changeable="false" ordered="false"
	 * @generated
	 */
	String getNumber();

	/**
	 * Returns the value of the '<em><b>Ordered</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ordered</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ordered</em>' attribute.
	 * @see Tiendavirtual.TiendavirtualPackage#getOrder_Ordered()
	 * @model unique="false" dataType="org.eclipse.uml2.types.String" required="true" changeable="false" ordered="false"
	 * @generated
	 */
	String getOrdered();

	/**
	 * Returns the value of the '<em><b>Shipped</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Shipped</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Shipped</em>' attribute.
	 * @see Tiendavirtual.TiendavirtualPackage#getOrder_Shipped()
	 * @model unique="false" dataType="org.eclipse.uml2.types.String" required="true" changeable="false" ordered="false"
	 * @generated
	 */
	String getShipped();

	/**
	 * Returns the value of the '<em><b>Ship to</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ship to</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ship to</em>' attribute.
	 * @see Tiendavirtual.TiendavirtualPackage#getOrder_Ship_to()
	 * @model unique="false" dataType="org.eclipse.uml2.types.String" required="true" changeable="false" ordered="false"
	 * @generated
	 */
	String getShip_to();

	/**
	 * Returns the value of the '<em><b>Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Status</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Status</em>' attribute.
	 * @see Tiendavirtual.TiendavirtualPackage#getOrder_Status()
	 * @model unique="false" dataType="org.eclipse.uml2.types.String" required="true" changeable="false" ordered="false"
	 * @generated
	 */
	String getStatus();

	/**
	 * Returns the value of the '<em><b>Total</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Total</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Total</em>' attribute.
	 * @see Tiendavirtual.TiendavirtualPackage#getOrder_Total()
	 * @model unique="false" dataType="org.eclipse.uml2.types.String" required="true" changeable="false" ordered="false"
	 * @generated
	 */
	String getTotal();

	/**
	 * Returns the value of the '<em><b></b></em>' reference list.
	 * The list contents are of type {@link Tiendavirtual.LineItem}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em></em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em></em>' reference list.
	 * @see Tiendavirtual.TiendavirtualPackage#getOrder__()
	 * @model type="Tiendavirtual.LineItem" required="true" changeable="false" ordered="false"
	 * @generated
	 */
	EList get_();

} // Order
