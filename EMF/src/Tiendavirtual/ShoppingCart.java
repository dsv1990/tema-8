/**
 */
package Tiendavirtual;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Shopping Cart</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link Tiendavirtual.ShoppingCart#getCreated <em>Created</em>}</li>
 *   <li>{@link Tiendavirtual.ShoppingCart#get_ <em></em>}</li>
 * </ul>
 * </p>
 *
 * @see Tiendavirtual.TiendavirtualPackage#getShoppingCart()
 * @model
 * @generated
 */
public interface ShoppingCart extends EObject {
	/**
	 * Returns the value of the '<em><b>Created</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Created</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Created</em>' attribute.
	 * @see Tiendavirtual.TiendavirtualPackage#getShoppingCart_Created()
	 * @model unique="false" dataType="org.eclipse.uml2.types.String" required="true" changeable="false" ordered="false"
	 * @generated
	 */
	String getCreated();

	/**
	 * Returns the value of the '<em><b></b></em>' reference list.
	 * The list contents are of type {@link Tiendavirtual.LineItem}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em></em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em></em>' reference list.
	 * @see Tiendavirtual.TiendavirtualPackage#getShoppingCart__()
	 * @model type="Tiendavirtual.LineItem" required="true" changeable="false" ordered="false"
	 * @generated
	 */
	EList get_();

} // ShoppingCart
