/**
 */
package Tiendavirtual;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Web User</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link Tiendavirtual.WebUser#getLogin_id <em>Login id</em>}</li>
 *   <li>{@link Tiendavirtual.WebUser#getPassword <em>Password</em>}</li>
 *   <li>{@link Tiendavirtual.WebUser#getState <em>State</em>}</li>
 *   <li>{@link Tiendavirtual.WebUser#get_ <em></em>}</li>
 * </ul>
 * </p>
 *
 * @see Tiendavirtual.TiendavirtualPackage#getWebUser()
 * @model
 * @generated
 */
public interface WebUser extends EObject {
	/**
	 * Returns the value of the '<em><b>Login id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Login id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Login id</em>' attribute.
	 * @see Tiendavirtual.TiendavirtualPackage#getWebUser_Login_id()
	 * @model unique="false" dataType="org.eclipse.uml2.types.String" required="true" changeable="false" ordered="false"
	 * @generated
	 */
	String getLogin_id();

	/**
	 * Returns the value of the '<em><b>Password</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Password</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Password</em>' attribute.
	 * @see Tiendavirtual.TiendavirtualPackage#getWebUser_Password()
	 * @model unique="false" dataType="org.eclipse.uml2.types.String" required="true" changeable="false" ordered="false"
	 * @generated
	 */
	String getPassword();

	/**
	 * Returns the value of the '<em><b>State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>State</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>State</em>' attribute.
	 * @see Tiendavirtual.TiendavirtualPackage#getWebUser_State()
	 * @model unique="false" dataType="org.eclipse.uml2.types.String" required="true" changeable="false" ordered="false"
	 * @generated
	 */
	String getState();

	/**
	 * Returns the value of the '<em><b></b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em></em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em></em>' reference.
	 * @see Tiendavirtual.TiendavirtualPackage#getWebUser__()
	 * @model changeable="false" ordered="false"
	 * @generated
	 */
	ShoppingCart get_();

} // WebUser
