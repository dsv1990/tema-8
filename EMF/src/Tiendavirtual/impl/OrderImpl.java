/**
 */
package Tiendavirtual.impl;

import Tiendavirtual.LineItem;
import Tiendavirtual.Order;
import Tiendavirtual.TiendavirtualPackage;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Order</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link Tiendavirtual.impl.OrderImpl#getNumber <em>Number</em>}</li>
 *   <li>{@link Tiendavirtual.impl.OrderImpl#getOrdered <em>Ordered</em>}</li>
 *   <li>{@link Tiendavirtual.impl.OrderImpl#getShipped <em>Shipped</em>}</li>
 *   <li>{@link Tiendavirtual.impl.OrderImpl#getShip_to <em>Ship to</em>}</li>
 *   <li>{@link Tiendavirtual.impl.OrderImpl#getStatus <em>Status</em>}</li>
 *   <li>{@link Tiendavirtual.impl.OrderImpl#getTotal <em>Total</em>}</li>
 *   <li>{@link Tiendavirtual.impl.OrderImpl#get_ <em></em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class OrderImpl extends EObjectImpl implements Order {
	/**
	 * The default value of the '{@link #getNumber() <em>Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumber()
	 * @generated
	 * @ordered
	 */
	protected static final String NUMBER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getNumber() <em>Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumber()
	 * @generated
	 * @ordered
	 */
	protected String number = NUMBER_EDEFAULT;

	/**
	 * The default value of the '{@link #getOrdered() <em>Ordered</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOrdered()
	 * @generated
	 * @ordered
	 */
	protected static final String ORDERED_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOrdered() <em>Ordered</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOrdered()
	 * @generated
	 * @ordered
	 */
	protected String ordered = ORDERED_EDEFAULT;

	/**
	 * The default value of the '{@link #getShipped() <em>Shipped</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getShipped()
	 * @generated
	 * @ordered
	 */
	protected static final String SHIPPED_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getShipped() <em>Shipped</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getShipped()
	 * @generated
	 * @ordered
	 */
	protected String shipped = SHIPPED_EDEFAULT;

	/**
	 * The default value of the '{@link #getShip_to() <em>Ship to</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getShip_to()
	 * @generated
	 * @ordered
	 */
	protected static final String SHIP_TO_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getShip_to() <em>Ship to</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getShip_to()
	 * @generated
	 * @ordered
	 */
	protected String ship_to = SHIP_TO_EDEFAULT;

	/**
	 * The default value of the '{@link #getStatus() <em>Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStatus()
	 * @generated
	 * @ordered
	 */
	protected static final String STATUS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getStatus() <em>Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStatus()
	 * @generated
	 * @ordered
	 */
	protected String status = STATUS_EDEFAULT;

	/**
	 * The default value of the '{@link #getTotal() <em>Total</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTotal()
	 * @generated
	 * @ordered
	 */
	protected static final String TOTAL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTotal() <em>Total</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTotal()
	 * @generated
	 * @ordered
	 */
	protected String total = TOTAL_EDEFAULT;

	/**
	 * The cached value of the '{@link #get_() <em></em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #get_()
	 * @generated
	 * @ordered
	 */
	protected EList _;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OrderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return TiendavirtualPackage.Literals.ORDER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getNumber() {
		return number;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getOrdered() {
		return ordered;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getShipped() {
		return shipped;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getShip_to() {
		return ship_to;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTotal() {
		return total;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList get_() {
		if (_ == null) {
			_ = new EObjectResolvingEList(LineItem.class, this, TiendavirtualPackage.ORDER__);
		}
		return _;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TiendavirtualPackage.ORDER__NUMBER:
				return getNumber();
			case TiendavirtualPackage.ORDER__ORDERED:
				return getOrdered();
			case TiendavirtualPackage.ORDER__SHIPPED:
				return getShipped();
			case TiendavirtualPackage.ORDER__SHIP_TO:
				return getShip_to();
			case TiendavirtualPackage.ORDER__STATUS:
				return getStatus();
			case TiendavirtualPackage.ORDER__TOTAL:
				return getTotal();
			case TiendavirtualPackage.ORDER__:
				return get_();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TiendavirtualPackage.ORDER__NUMBER:
				return NUMBER_EDEFAULT == null ? number != null : !NUMBER_EDEFAULT.equals(number);
			case TiendavirtualPackage.ORDER__ORDERED:
				return ORDERED_EDEFAULT == null ? ordered != null : !ORDERED_EDEFAULT.equals(ordered);
			case TiendavirtualPackage.ORDER__SHIPPED:
				return SHIPPED_EDEFAULT == null ? shipped != null : !SHIPPED_EDEFAULT.equals(shipped);
			case TiendavirtualPackage.ORDER__SHIP_TO:
				return SHIP_TO_EDEFAULT == null ? ship_to != null : !SHIP_TO_EDEFAULT.equals(ship_to);
			case TiendavirtualPackage.ORDER__STATUS:
				return STATUS_EDEFAULT == null ? status != null : !STATUS_EDEFAULT.equals(status);
			case TiendavirtualPackage.ORDER__TOTAL:
				return TOTAL_EDEFAULT == null ? total != null : !TOTAL_EDEFAULT.equals(total);
			case TiendavirtualPackage.ORDER__:
				return _ != null && !_.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (number: ");
		result.append(number);
		result.append(", ordered: ");
		result.append(ordered);
		result.append(", shipped: ");
		result.append(shipped);
		result.append(", ship_to: ");
		result.append(ship_to);
		result.append(", status: ");
		result.append(status);
		result.append(", total: ");
		result.append(total);
		result.append(')');
		return result.toString();
	}

} //OrderImpl
