/**
 */
package Tiendavirtual.impl;

import Tiendavirtual.Account;
import Tiendavirtual.Payment;
import Tiendavirtual.ShoppingCart;
import Tiendavirtual.TiendavirtualPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Account</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link Tiendavirtual.impl.AccountImpl#getId <em>Id</em>}</li>
 *   <li>{@link Tiendavirtual.impl.AccountImpl#getBilling_address <em>Billing address</em>}</li>
 *   <li>{@link Tiendavirtual.impl.AccountImpl#getIs_closed <em>Is closed</em>}</li>
 *   <li>{@link Tiendavirtual.impl.AccountImpl#getOpen <em>Open</em>}</li>
 *   <li>{@link Tiendavirtual.impl.AccountImpl#getClosed <em>Closed</em>}</li>
 *   <li>{@link Tiendavirtual.impl.AccountImpl#get_ <em></em>}</li>
 *   <li>{@link Tiendavirtual.impl.AccountImpl#getPayment <em>Payment</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class AccountImpl extends EObjectImpl implements Account {
	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getBilling_address() <em>Billing address</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBilling_address()
	 * @generated
	 * @ordered
	 */
	protected static final String BILLING_ADDRESS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getBilling_address() <em>Billing address</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBilling_address()
	 * @generated
	 * @ordered
	 */
	protected String billing_address = BILLING_ADDRESS_EDEFAULT;

	/**
	 * The default value of the '{@link #getIs_closed() <em>Is closed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIs_closed()
	 * @generated
	 * @ordered
	 */
	protected static final String IS_CLOSED_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getIs_closed() <em>Is closed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIs_closed()
	 * @generated
	 * @ordered
	 */
	protected String is_closed = IS_CLOSED_EDEFAULT;

	/**
	 * The default value of the '{@link #getOpen() <em>Open</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOpen()
	 * @generated
	 * @ordered
	 */
	protected static final String OPEN_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOpen() <em>Open</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOpen()
	 * @generated
	 * @ordered
	 */
	protected String open = OPEN_EDEFAULT;

	/**
	 * The default value of the '{@link #getClosed() <em>Closed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClosed()
	 * @generated
	 * @ordered
	 */
	protected static final String CLOSED_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getClosed() <em>Closed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClosed()
	 * @generated
	 * @ordered
	 */
	protected String closed = CLOSED_EDEFAULT;

	/**
	 * The cached value of the '{@link #get_() <em></em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #get_()
	 * @generated
	 * @ordered
	 */
	protected ShoppingCart _;

	/**
	 * The cached value of the '{@link #getPayment() <em>Payment</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPayment()
	 * @generated
	 * @ordered
	 */
	protected EList payment;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AccountImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return TiendavirtualPackage.Literals.ACCOUNT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getBilling_address() {
		return billing_address;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getIs_closed() {
		return is_closed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getOpen() {
		return open;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getClosed() {
		return closed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShoppingCart get_() {
		return _;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSet_(ShoppingCart new_, NotificationChain msgs) {
		ShoppingCart old_ = _;
		_ = new_;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TiendavirtualPackage.ACCOUNT__, old_, new_);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getPayment() {
		if (payment == null) {
			payment = new EObjectResolvingEList(Payment.class, this, TiendavirtualPackage.ACCOUNT__PAYMENT);
		}
		return payment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TiendavirtualPackage.ACCOUNT__:
				return basicSet_(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TiendavirtualPackage.ACCOUNT__ID:
				return getId();
			case TiendavirtualPackage.ACCOUNT__BILLING_ADDRESS:
				return getBilling_address();
			case TiendavirtualPackage.ACCOUNT__IS_CLOSED:
				return getIs_closed();
			case TiendavirtualPackage.ACCOUNT__OPEN:
				return getOpen();
			case TiendavirtualPackage.ACCOUNT__CLOSED:
				return getClosed();
			case TiendavirtualPackage.ACCOUNT__:
				return get_();
			case TiendavirtualPackage.ACCOUNT__PAYMENT:
				return getPayment();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TiendavirtualPackage.ACCOUNT__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case TiendavirtualPackage.ACCOUNT__BILLING_ADDRESS:
				return BILLING_ADDRESS_EDEFAULT == null ? billing_address != null : !BILLING_ADDRESS_EDEFAULT.equals(billing_address);
			case TiendavirtualPackage.ACCOUNT__IS_CLOSED:
				return IS_CLOSED_EDEFAULT == null ? is_closed != null : !IS_CLOSED_EDEFAULT.equals(is_closed);
			case TiendavirtualPackage.ACCOUNT__OPEN:
				return OPEN_EDEFAULT == null ? open != null : !OPEN_EDEFAULT.equals(open);
			case TiendavirtualPackage.ACCOUNT__CLOSED:
				return CLOSED_EDEFAULT == null ? closed != null : !CLOSED_EDEFAULT.equals(closed);
			case TiendavirtualPackage.ACCOUNT__:
				return _ != null;
			case TiendavirtualPackage.ACCOUNT__PAYMENT:
				return payment != null && !payment.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(", billing_address: ");
		result.append(billing_address);
		result.append(", is_closed: ");
		result.append(is_closed);
		result.append(", open: ");
		result.append(open);
		result.append(", closed: ");
		result.append(closed);
		result.append(')');
		return result.toString();
	}

} //AccountImpl
